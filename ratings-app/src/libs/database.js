export const articleDatabase = [
    { articleCode: 'DEL', title: 'DELARTE',         kitchenType: 'Italien' ,  address: '20 Boulevard Saint-Michel', postaleCode: '75006', city: 'Paris' , averagesComments: 0},
    // { articleCode: 'BRI', title: 'BRIOCHE DOREE',   kitchenType: 'Français',  address: '80 Rue de Rivoli',          postaleCode: '75004', city: 'Paris' , averagesComments: 0},
    // { articleCode: 'BUF', title: 'BUFFALO GRILL',   kitchenType: 'Américain', address: '15 Place de la République', postaleCode: '75003', city: 'Paris' , averagesComments: 0},
    // { articleCode: 'DOM', title: 'DOMNIOS PIZZA',   kitchenType: 'Italien',   address: '14 Rue Saint-Marc',         postaleCode: '75002', city: 'Paris' , averagesComments: 0},
];

export function findArticle(value)
{
    // Retourne l'article recherché    
    return articleDatabase.find(article => article.articleCode === value ) ?? false;
}

export function setAveragesCommentsArticles(index, valueAveragesComments)
{
    articleDatabase[index].averagesComments = valueAveragesComments;
}


