import React from "react";
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

import { NavBar } from './components/NavBar';
import { Home } from './pages/Home';
import { DetailArticle } from './pages/DetailArticle';

import { AppContext } from './AppContext';
import { articleDatabase } from "./libs/database";


class App extends React.Component {

  state = {
    rating : localStorage.getItem('rating'),
    articles: articleDatabase
  };

  handleChangeFilterArticle = (e) => {
      e.preventDefault();

      const value = e.target.value; 
      const articles = [...articleDatabase];

      const filterArticle = ( value !== 'all' ) ? articles.filter(article => article.averagesComments === parseInt(value)) : articles;
  
      console.log(value);
      console.log('---');
      console.log(articles);
      console.log('---');
      console.log(filterArticle);
      console.log('---');
      


      this.setState({articles: filterArticle});
      
  };

  getArticle(code) {
    return this.state.articles.find(article => article.articleCode === code );
  }

  render(){

    return ( 

      <AppContext.Provider value={this.state}>       
       <BrowserRouter> 
           <NavBar />   

         <Switch>              
           <Route exact path="/">                
             <Home articles={this.state.articles} onChangeFilterArticle={this.handleChangeFilterArticle}/>
           </Route>
           <Route exact path="/comment/show/:code" loadArticle={this.getArticle}>
             <DetailArticle />
           </Route>
        
         </Switch> 
         </BrowserRouter>
      </AppContext.Provider>
   
    
    );
    
  }
  
}

export default App;




