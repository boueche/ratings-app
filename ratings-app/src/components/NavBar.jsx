import React from 'react';
import {
    NavLink,
} from "react-router-dom";

import './navbar.css';

export function NavBar (props)
{
   
    return (
                <nav className="navbar navbar-expand-lg navbar-light">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation-bar">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navigation-bar">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <NavLink className="nav-link" exact to="/">RATING APP</NavLink>
                            </li>          
                        </ul>
                    </div>                   
                </nav>    
            );   
}