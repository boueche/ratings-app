import React from "react";


import './articleShow.css';
import { articleDatabase, setAveragesCommentsArticles } from "../libs/database";
import { ListAssessment } from './ListAssessment';


export class ArticleShow extends React.Component{


    state = {  
        rates: (localStorage.getItem("rates") !== null) ? JSON.parse(localStorage.getItem('rates')) : [],
        countComments: this.getCountComment(),
        scoreComments : this.getScoreComment(),    
        user: "",
        comment: "",
        rating: 0,
        code: this.props.code,
        showForm : false        
    }

    getCountComment(){

        var count = 0;
        const rates = (localStorage.getItem("rates") !== null) ? JSON.parse(localStorage.getItem('rates')) : [];
        const comments = rates.values();        
        for( const comment of comments ){
            if( comment.code === this.props.code ){
                count++;
            }
        }
        return count;
    }

    getScoreComment(){

        var score = 0;
        const rates = (localStorage.getItem("rates") !== null) ? JSON.parse(localStorage.getItem('rates')) : [];
        const comments = rates.values();        
        for( const comment of comments ){
            if( comment.code === this.props.code ){
                score += parseInt(comment.rating);
            }
        }
        return score;      
    }

    getCode(){
        return this.state.code;
    }

    getAverage(){
      
        var average = Number.parseFloat(this.state.scoreComments / this.state.countComments).toFixed(2);
        if( isNaN(average)){
            average = 0;
        }    
        if(average.toString().indexOf('.') !== -1 ){
            var isDecZero =  average.split('.')[1];
            if( isDecZero === '00'){
                average = parseInt(average);               
            }
        }      
        return average;
    }

    checkInputIsEmpty(str){
        var result = true;
        if( !str.replace(/\s+/, '').length ) {            
            result = false;
        }
        return result;
    }

    handleClickRating = (event) => {
        event.preventDefault();

        const content = document.getElementsByClassName('content-rating')[0];
        const display = content.getAttribute('class').replace(' content-rating','');

        if( display === 'd-none'){
            this.setState({showForm: true });
        }else{
            this.setState({showForm: false });
        }
    }

    handleChangeInput = (event) => {
        event.preventDefault();
        this.setState({[event.target.name]: event.target.value}); 
    }

    handleChangeRating = ( event ) => {
        event.preventDefault();
        this.setState({rating: event.target.dataset.id});
    }

    handleSubmitRating = (e) => {
        e.preventDefault();

        const { user, comment , rating, code } = this.state;    
        
        // Vérification des saisies des champs.
        if( !this.checkInputIsEmpty(user) ){            
            alert(`Saisissez votre nom d'utilisateur.`);
            return false;
        }
        if( !this.checkInputIsEmpty(comment) ){            
            alert(`Saisissez votre commentaire.`);
            return false;
        }
   
        if( rating === 0 ){            
            alert(`Selectionnez une note.`);
            return false;
        }        

        // Verfie si le local storage 'rates' est défini.        
        if (localStorage.getItem("rates") !== null) {  
            this.setState({rates: JSON.parse(localStorage.getItem('rates'))});
        }
    
        // Création de la ligne de commentaire enregistré.
        const line = { user:user, comment:comment, rating:rating, code:code };
        const rates = this.state.rates;
        rates.push(line);
        this.setState({rates: rates});

        // Mise à jour des indicateurs commentaires
        
        // Compteur
        this.setState({ countComments: this.state.countComments + 1});
      
        // Note
        const comments = this.state.rates.values();        
        for( const comment of comments ){
            if( comment.code === this.props.code ){
                this.setState({ scoreComments: parseInt(this.state.scoreComments) + parseInt(comment.rating) }, () => {
                    const indexArticles = articleDatabase.findIndex((article) => article.articleCode === this.props.code );                
                    setAveragesCommentsArticles( indexArticles , this.getAverage() );
                });
               
            }
        }

        localStorage.setItem('rates', JSON.stringify( this.state.rates));
        e.target.reset();
    }
       
    render()
    {   
        const article = this.props.article;
     
        const assessmentLines = this.state.rates.map((rateLine, index) => 
        {
            if( rateLine.code === this.props.code ){
                const { user, comment , rating, code } = rateLine; 
                return <ListAssessment key={ index } user={user} comment={comment} rating={rating} code={code} />;
            }else{
                return false;
            }

        });

        return (   
            <section className="articles-list row col-4">
                 <article className="card mt-2 " >
                   
                   <div className="card-body d-flex">
                       <div className="col-12">
                           <img src={`/images/articles/${article.articleCode.toLowerCase()}.jpg`} className="card-img-top" alt={ article.title} />
                       </div>
                       <div className="col-12 border-top mt-4 mb-4">
                           <h4 className="blue">{ article.title}</h4>
                           <div className="d-flex flex-row align-items-center comment">
                               <span className="averagesComment pr-1">{ this.getAverage() }</span>
                               <div className="rating">
                                    <a href="#5" className={`${ Math.round(this.getAverage()) >= 5 ? 'active' : '' } `} title="Donner 5 étoiles">☆</a>
                                    <a href="#4" className={`${ Math.round(this.getAverage()) >= 4 ? 'active' : '' } `} title="Donner 4 étoiles">☆</a>
                                    <a href="#3" className={`${ Math.round(this.getAverage()) >= 3 ? 'active' : '' } `} title="Donner 3 étoiles">☆</a>
                                    <a href="#2" className={`${ Math.round(this.getAverage()) >= 2 ? 'active' : '' } `} title="Donner 2 étoiles">☆</a>
                                    <a href="#1" className={`${ Math.round(this.getAverage()) >= 1 ? 'active' : '' } `} title="Donner 1 étoile">☆</a>
                               </div>
                               <span className="countComment pl-1">({ this.state.countComments })</span>
                           </div>                            
                           <div className="category">
                                <span>Cuisine : <i>{ article.kitchenType }</i></span><br/>
                                <small>{ article.address }</small><br/>
                                <small>{article.postaleCode} - { article.city }</small>
                           </div>                           
                       </div>

                       <div className="col-12 border-top mt-4 mb-4 d-flex justify-content-center mb-4">
                            <button className="btn btn-outline-info btn-sm mt-4" onClick={this.handleClickRating} data-code={this.props.code}>Rédiger votre avis</button>    
                       </div>
                      
                       <div className={`${this.state.showForm ? 'd-flex' : 'd-none'} content-rating`} >
                            <form action={`/comment/show/${this.state.code }`} className="col-12" onSubmit={this.handleSubmitRating} >
                                <div className="form-group">
                                    <label htmlFor="user">Votre nom d'utilisateur</label>
                                    <input type="text" className="form-control form-control-sm" name="user" id="user" placeholder="Votre nom d'utilisateur" onChange={this.handleChangeInput} />                                    
                                </div>
                                <div className="form-group">
                                    <label htmlFor="comment">Votre commentaire</label>
                                    <textarea className="form-control form-control-sm" id="comment" name="comment" placeholder="Votre commentaire" onChange={this.handleChangeInput}></textarea>
                                </div>
                                <div className="form-group">                                   
                                    <label className="form-check-label" htmlFor="rating">Note</label>
                                    <div className="rating">             
                                        <a href="#5" data-id="5" onClick={this.handleChangeRating} className="" title="Donner 5 étoiles">☆</a>
                                        <a href="#4" data-id="4" onClick={this.handleChangeRating} className="" title="Donner 4 étoiles">☆</a>
                                        <a href="#3" data-id="3" onClick={this.handleChangeRating} className="" title="Donner 3 étoiles">☆</a>
                                        <a href="#2" data-id="2" onClick={this.handleChangeRating} className="" title="Donner 2 étoiles">☆</a>
                                        <a href="#1" data-id="1" onClick={this.handleChangeRating} className="" title="Donner 1 étoile">☆</a>
                                    </div>
                                </div>                               
                                <button type="submit" className="btn btn-primary btn-sm float-right">Voter</button>
                            </form>
                       </div>
                       
                       <div className="col-12 mt-4 mb-4 d-flex justify-content-center mb-4 flex-column">
                            { (assessmentLines === false) ? 'Aucun commentaire' : assessmentLines }
                       </div>

                   </div>
               </article>
            </section>
        );
    }
}