import React from "react";


import './listAssessment.css';


export class ListAssessment extends React.Component{

    getAverage(){
      
        var average = Number.parseFloat(this.props.rating).toFixed(2);
        if( isNaN(average)){
            average = 0;
        }    
        return average;
    }

    render()
    { 
        return (
                <div className="col-12 border border-gray mb-2">
                    <div className="row d-flex align-items-baseline mt-3 ">
                        <div className="col-3">
                            <img className="rounded-circle" src="https://via.placeholder.com/50x50.png" width="40" height="40" alt="icone_user"/>
                        </div>
                        <div className="col-8">
                            <h6 className="text-capitalize">{ this.props.user }</h6>
                        </div>
                    </div>
                    <div className="col-12 px-0">
                        <div className="d-flex flex-row align-items-center comment">                                        
                            <div className="rating">
                                <a href="#5" className={`${ Math.round(this.getAverage()) >= 5 ? 'active' : '' } `} title="Donner 5 étoiles">☆</a>
                                <a href="#4" className={`${ Math.round(this.getAverage()) >= 4 ? 'active' : '' } `} title="Donner 4 étoiles">☆</a>
                                <a href="#3" className={`${ Math.round(this.getAverage()) >= 3 ? 'active' : '' } `} title="Donner 3 étoiles">☆</a>
                                <a href="#2" className={`${ Math.round(this.getAverage()) >= 2 ? 'active' : '' } `} title="Donner 2 étoiles">☆</a>
                                <a href="#1" className={`${ Math.round(this.getAverage()) >= 1 ? 'active' : '' } `} title="Donner 1 étoile">☆</a>
                            </div>                                        
                        </div>  
                    </div>
                    <p className="text-justify font-italic small">{ this.props.comment }</p>                               
                </div>
        );
    }    
}





