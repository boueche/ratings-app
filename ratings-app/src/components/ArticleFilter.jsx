import React from "react";

import { AppContext } from '../AppContext'; 

import './articleFilter.css';

export class ArticleFilter extends React.Component{

    // Enfant du context
    static contextType = AppContext;
    
    state = {
        'rating': [{id:1}, {id:2},{id:3},{id:4},{id:5}]
    };

    render(){

        const filter = this.state.rating.map((rating, index) =>
        {
            return (              
                    <option key={index} value={rating.id}>
                        {` ${rating.id} étoiles(s)`}
                    </option>
            );
        });      
      
        return (   
            <section className="articles-filter row col-12">
                <article className="col-12 d-flex flex-center align-items-center mt-2 px-0" >
                    <form className="form-inline col-12"> 
                         <div className="form-group col-12 px-0">
                            <label htmlFor="article-filter " className="col-1 col-form-label">Note </label>
                            <div className="col-10">
                                <select className="form-control form-control-sm" id="article-filter" onChange={this.props.onchangeFilter}>
                                    <option value="all">Toutes</option>
                                    { filter }
                                </select>   
                            </div> 
                        </div>      
                    </form>
                </article>
            </section>
        );
    }
}