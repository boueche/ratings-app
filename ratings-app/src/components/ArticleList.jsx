import React from 'react';
import {
    NavLink,
} from "react-router-dom";

// import { AppContext } from '../AppContext'; 

import './articleList.css';

export class ArticleList extends React.Component
{   
    state = {  
        rates: (localStorage.getItem("rates") !== null) ? JSON.parse(localStorage.getItem('rates')) : [],
        countComments: 0,
        scoreComments : 0
    }

    getCountComment(articleCode){

        var count = 0;
        const rates = (localStorage.getItem("rates") !== null) ? JSON.parse(localStorage.getItem('rates')) : [];
        const comments = rates.values();        
        for( const comment of comments ){
            if( comment.code === articleCode ){
                count++;
            }
        }
        return count;
    }

    getScoreComment(articleCode){

        var score = 0;
        const rates = (localStorage.getItem("rates") !== null) ? JSON.parse(localStorage.getItem('rates')) : [];
        const comments = rates.values();        
        for( const comment of comments ){
            if( comment.code === articleCode ){
                score += parseInt(comment.rating);
            }
        }
        return score;      
    }

    getAverage(articleCode){

        var average = Number.parseFloat( this.getScoreComment(articleCode) / this.getCountComment(articleCode)).toFixed(2);
       
        // Vérification de la valeur de la moyenne.
        if( isNaN(average)){
            average = 0;
        }

        if(average.toString().indexOf('.') !== -1 ){
            var isDecZero =  average.split('.')[1];
            if( isDecZero === '00'){
                average = parseInt(average);               
            }
        }        
        
        return average;
    }

    render()
    {            
        const cards = this.props.database.map((article, index) => 
        {    
            return (
                <article key={index} className="card mt-2 " >
                   
                    <div className="card-body d-flex flex-row">
                        <div className="col-8">
                            <NavLink className="card-text text-decoration-none" exact to={`/comment/show/${article.articleCode}`}>{ article.title}</NavLink>
                            <div className="d-flex flex-row align-items-center comment">
                                <span className="averagesComment pr-1">{ this.getAverage(article.articleCode) }</span>
                                <div className="rating">
                                    <a href="#5" className={`${ Math.round(this.getAverage(article.articleCode)) >= 5 ? 'active' : '' } `}  title="Donner 5 étoiles">☆</a>
                                    <a href="#4" className={`${ Math.round(this.getAverage(article.articleCode)) >= 4 ? 'active' : '' } `}  title="Donner 4 étoiles">☆</a>
                                    <a href="#3" className={`${ Math.round(this.getAverage(article.articleCode)) >= 3 ? 'active' : '' } `}  title="Donner 3 étoiles">☆</a>
                                    <a href="#2" className={`${ Math.round(this.getAverage(article.articleCode)) >= 2 ? 'active' : '' } `}  title="Donner 2 étoiles">☆</a>
                                    <a href="#1" className={`${ Math.round(this.getAverage(article.articleCode)) >= 1 ? 'active' : '' } `}  title="Donner 1 étoile">☆</a>
                                </div>
                                <span className="countComment pl-1">({ this.getCountComment(article.articleCode) })</span>
                            </div>                            
                            <div className="category">
                                <span>Cuisine : <i>{ article.kitchenType }</i></span><br/>
                                <small>{ article.address }</small><br/>
                                <small>{article.postaleCode} - { article.city }</small>
                            </div>                           
                        </div>
                        <div className="col-4">
                            <img src={`/images/articles/${article.articleCode.toLowerCase()}.jpg`} className="card-img-top" alt={ article.title} />
                        </div>
                    </div>
                </article>
            );
        });

        return (   
            <section className="articles-list row col-12">{ cards }</section>
        );
    }
}

