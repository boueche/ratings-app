import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

// import Geocode from "react-geocode";

import { articleDatabase } from '../libs/database';

import './mapgoogle.css';
 
// const AnyReactComponent = ({ text }) => <> <div className="pin"></div><div className="pulse"></div></>;
const AnyReactComponent = ({ text }) => <div className="">{text}</div>;
 
export class MapGoogle extends Component {

    constructor(props) {
        super(props);
        this.state = {
            api : '******************************',        
            coordinate : [],
            markers : []
        };
        // La ligne ci-dessous est importante !
        this.getCoordinate = this.getCoordinate.bind(this);
    }

   

    static defaultProps = {
        center: {
            lat: 48.8534,
            lng: 2.3488
        },
        zoom: 13
    };

    // async getLatLtnByAddress( address, code )
    // async tLatLtnByAddress( address, code )
    // {         
    //    var result = { status: false, lat: null, lng: null};

        // set Google Maps Geocoding API for purposes of quota management. Its optional but recommended.
        // Geocode.setApiKey(this.state.api);        

        // set response language. Defaults to english.
        // Geocode.setLanguage("fr");

        // set response region. Its optional.
        // A Geocoding request with region=es (Spain) will return the Spanish city.
        // Geocode.setRegion("fr");

        // Enable or disable logs. Its optional.
        // Geocode.enableDebug();

        // Get latitude & longitude from address.
        // const result = Geocode.fromAddress( address ).then(
        //                     response => {                                            
        //                         const { lat, lng } = response.results[0].geometry.location;                                 
        //                         this.setState({coordinate: {status: true, lat: lat, lng: lng}});                                                        
        //                     },
        //                     error => {
        //                         // console.error(error);        
        //                         this.setState({coordinate: {status: false, lat: null, lng: null}});                  
        //                     }
        //                 );
        // const result =  Geocode.fromAddress(address);
        // const response = await result.then(function(r) {
        //     console.log(r); // "Some User token"
        //  })

        // console.log('***');
        // console.log(result);
        // console.log(response);
        // console.log('***');
        // return result;
    // }  

    getCoordinate( address )
    {
        const encodedAddress = encodeURI( address);       
        // fetches data from our api
        const position =  window.fetch(`https://maps.googleapis.com/maps/api/geocode/json?address==${encodedAddress},+CA&key=${this.state.api }`, {
                                "method": "GET",
                            })        
                            .then(response => response.json())
                            .then(response => {
                                if( response.status === 'OK'){
                                    const { lat, lng } = response.results[0].geometry.location; 
                                    const coordinate = {lat:lat,lng:lng};
                                    return coordinate;
                                }                     
                            })
                            .catch(err => console.log(err));       

        return position;
    }

    async loadCoordinate(address){
        var pos = await this.getCoordinate( address );
        return pos;
    }

    componentDidMount(){

        const markers = articleDatabase.map((article, index) => {
                const position = this.loadCoordinate(article.address +', '+ article.postaleCode + ', '+ article.city)
                                .then(pos => {
                                   console.log( pos );
                                });
                console.log('position', position);

            // return <AnyReactComponent key={index} lat={r.lat} lng={r.lng} text={article.title} />
          
           
                                        
        });
        
        this.setState({markers: markers});
    }
 
    render() {
     
        // console.log('before');
        // console.log(this.state.markers);
        // console.log('after');
        
        return (
            // Important! Always set the container height explicitly
            <div style={{ height: '100vh', width: '100%' }}>
            <GoogleMapReact
                bootstrapURLKeys={{ key: this.state.api }}
                defaultCenter={this.props.center}
                defaultZoom={this.props.zoom}
            >
            {this.state.markers}
            </GoogleMapReact>
            </div>
        );
    }
}
 
// export default MapGoogle;
