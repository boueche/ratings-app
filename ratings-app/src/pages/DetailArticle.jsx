import React from 'react';
import { Redirect, NavLink} from "react-router-dom";

import { findArticle } from '../libs/database';
import { ArticleShow } from '../components/ArticleShow';


import './home.css';

export class DetailArticle extends React.Component
{
    state = {
        // code : this.props.match.params.code,
        code : window.location.href.split('/').slice(-1).pop(),
        redirect : "/"       
    }

    render(){

        if( findArticle(this.state.code) !== false ){
            return (
                <main>
                    <div className="col-12 d-flex justify-content-between align-items-end">
                        <h6 className="mt-4">Fiche article</h6>
                        <NavLink className="btn btn-outline-secondary btn-sm" exact to="/">Retour</NavLink>
                    </div>                   
                    <hr/>
                    <ArticleShow code={this.state.code} article={findArticle(this.state.code)}/>
                    <div id="google-map" className="col-8">Google Map</div>
                </main>
            );
        }else{
            return <Redirect to={this.state.redirect} />
        }
       
      
    }
  
}