import React from 'react';

import { ArticleFilter } from '../components/ArticleFilter';
import { ArticleList } from '../components/ArticleList';
import { MapGoogle } from '../components/MapGoogle';


import './home.css';

export function Home(props)
{
    return (
        <main>
            <h6 className="mt-4 ml-4">Accueil</h6>
            <hr/>
            <div className="row">
                <div className="col-4">
                    <ArticleFilter database={ props.articles } onchangeFilter={props.onChangeFilterArticle}/>
                    <ArticleList database={ props.articles } />
                </div>
                <div className="col-8">
                    <MapGoogle />
                </div>               
            </div>

        </main>
        
    );
}